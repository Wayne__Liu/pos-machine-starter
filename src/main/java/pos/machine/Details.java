package pos.machine;

public class Details {

    private String barcode;
    private String name;

    private int price;

    public Details(String barcode) {
        this.barcode = barcode;
    }

    private int number;

    private int Subtotal;

    public Details(String name, int price, int number, int subtotal) {
        this.name = name;
        this.price = price;
        this.number = number;
        Subtotal = subtotal;
    }

    public Details() {

    }

    public String getName() {
        return name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(int subtotal) {
        Subtotal = subtotal;
    }
}
