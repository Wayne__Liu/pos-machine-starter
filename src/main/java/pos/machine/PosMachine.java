package pos.machine;

import java.util.List;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        RegisterSystem registerSystem = new RegisterSystem();
        String receipt = registerSystem.generateReceipt(barcodes);
        return receipt;
    }
}
