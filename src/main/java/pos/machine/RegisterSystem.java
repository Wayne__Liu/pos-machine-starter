package pos.machine;

import java.util.*;

public class RegisterSystem {
    public String generateReceipt(List<String> barcodes) {
        List<String> barcodesWithoutError = isVaild(barcodes);
        List<Details> productData = getProductData(barcodesWithoutError);
        StringBuilder sb = new StringBuilder();
        String LastName = null;
        sb.append("***<store earning no money>Receipt***\n");
        for (Object obj : productData) {
            if (LastName != ((Details) obj).getName() || LastName == null) {
                sb.append("Name: ").append(((Details) obj).getName()).append(", ");
                sb.append("Quantity: ").append(((Details) obj).getNumber()).append(", ");
                sb.append("Unit price: ").append(((Details) obj).getPrice()).append(" (yuan), ");
                sb.append("Subtotal: ").append(((Details) obj).getSubtotal()).append(" (yuan)\n");
                LastName = ((Details) obj).getName();
            }
        }
        sb.append("----------------------\n");
        sb.append("Total: ").append(getProductTotal(barcodesWithoutError)).append(" (yuan)\n");
        sb.append("**********************");
        return sb.toString();
    }

    public Integer getProductTotal(List<String> barcodes) {
        List<String> barcodesWithoutError = isVaild(barcodes);
        List<Details> productData = getProductData(barcodesWithoutError);
        String LastName = null;
        Integer Total = 0;
        for (Object obj : productData) {
            if (LastName != ((Details) obj).getName() || LastName == null) {
                Total = Total + ((Details) obj).getSubtotal();
                LastName = ((Details) obj).getName();
            }
        }
        return Total;
    }

    public List<String> isVaild(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();
        List<String> barcodesWithoutError = new ArrayList<>();
        for (String barcode : barcodes) {
            for (Item item : items) {
                if (item.getBarcode().equals(barcode)) {
                    barcodesWithoutError.add(barcode);
                }
            }
        }
        return barcodesWithoutError;
    }

    public List<Details> getProductData(List<String> barcodes) {
        Map<String, String> productNameMap = getProductName(barcodes);
        List<Details> detailsList = new ArrayList<>();
        for (int i = 0; i < barcodes.size(); i++) {
            Details productDetails = new Details();
            String productName = productNameMap.get(barcodes.get(i));
            productDetails.setName(productName);
            productDetails.setBarcode(barcodes.get(i));
            Map<String, Integer> productNumber = getProductNum(barcodes);
            Integer productNum = productNumber.get(barcodes.get(i));
            productDetails.setNumber(productNum);
            Map<String, Integer> productUnitPrice = getProductUnitPrice(barcodes);
            Integer productPrice = productUnitPrice.get(barcodes.get(i));
            productDetails.setPrice(productPrice);
            Map<String, Integer> productSubtotal = getProductSubtotal(barcodes);
            Integer productSubtotalPrice = productSubtotal.get(barcodes.get(i));
            productDetails.setSubtotal(productSubtotalPrice);
            detailsList.add(productDetails);
        }
        return detailsList;
    }

    public Map<String, Integer> getProductNum(List<String> barcodes) {
        Map<String, Integer> barcodeCountMap = new HashMap<>();
        for (String barcode : barcodes) {
            int count = barcodeCountMap.getOrDefault(barcode, 0);
            barcodeCountMap.put(barcode, count + 1);
        }
        return barcodeCountMap;
    }

    public Map<String, String> getProductName(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();
        Map<String, String> productNameMap = new HashMap<>();
        for (String barcode : barcodes) {
            for (Item item : items) {
                if (item.getBarcode().equals(barcode)) {
                    productNameMap.put(barcode, item.getName());
                }
            }
        }
        return productNameMap;
    }

    public Map<String, Integer> getProductUnitPrice(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();
        Map<String, Integer> productUnitPriceMap = new HashMap<>();
        for (String barcode : barcodes) {
            for (Item item : items) {
                if (item.getBarcode().equals(barcode)) {
                    productUnitPriceMap.put(barcode, item.getPrice());
                }
            }
        }
        return productUnitPriceMap;
    }

    public Map<String, Integer> getProductSubtotal(List<String> barcodes) {
        Map<String, Integer> productUnitPrice = getProductUnitPrice(barcodes);
        Map<String, Integer> productNumber = getProductNum(barcodes);
        Map<String, Integer> productSubtotalMap = new HashMap<>();
        for (String barcode : barcodes) {
            Integer productNum = productNumber.get(barcode);
            Integer productPrice = productUnitPrice.get(barcode);
            productSubtotalMap.put(barcode, productNum * productPrice);
        }
        return productSubtotalMap;
    }

}
